#!/usr/bin/env node

const archiver = require('archiver');
const readdirp = require('readdirp');
const fs = require('fs');
var md5 = require('md5');
const chalk = require('chalk');
const program = require('commander');
const version = require('../package.json').version;

console.log(__dirname);

program.version(version);
program
  .command('package')
  .description('Start packaging the module zip file')
  .option('-d, --asset-dir <path>', 'set the asset folder path')
  .option('-n, --output-name <name>', 'set output name')
  .action(package);

const args = process.argv;
if (args.length < 3) {
  console.log(chalk.red('No command is provided. Please run "ituni package"'));
}

program.parse(args);

async function package(command) {
  console.log(chalk.bold('Start packaging'));
  const { assetDir, outputName } = command;

  if (!assetDir) {
    console.log(chalk.red(`error: You must specify the assset directory.`));
    console.log('\n', 'usage: ituni package -d <assetDir>', '\n');
  } else {
    const fileName = outputName || 'ituni-module.zip';
    const output = fs.createWriteStream(fileName);
    output.on('close', async () => {
      const md5 = await hashMd5(fileName);
      console.log(chalk`{green success} MD5: ${md5}`);
    });
    const archive = archiver('zip', {
      zlib: { level: 9 } // Sets the compression level.
    });

    archive.pipe(output);

    const settings = {
      root: assetDir,
      entryType: 'all',
      depth: 0,
      fileFilter: ['!cordova.js', '!cordova_plugins.js'],
      directoryFilter: ['!cordova-js-src', '!plugins']
    };
    readdirp(
      settings,
      fileInfo => {
        if (fileInfo.stat.isDirectory()) {
          console.log(chalk`{yellow folder} ${fileInfo.name}`);
          archive.directory(fileInfo.fullPath, `assets/www/${fileInfo.name}`);
        } else {
          console.log(chalk`{blue file} ${fileInfo.name}`);
          archive.file(fileInfo.fullPath, { name: `assets/www/${fileInfo.name}` });
        }
      },
      err => {
        if (err) {
          throw err;
        }

        archive.finalize();
        console.log(chalk`{green success} Finished packaging {greenBright ${fileName}}`);
      }
    );
  }
}

function hashMd5(filePath) {
  return new Promise((resolve, reject) => {
    fs.readFile(filePath, function(err, buf) {
      if (err) {
        reject(err);
      }
      resolve(md5(buf));
    });
  });
}
