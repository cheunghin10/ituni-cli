# Hi, stolen from Edward =)

# ITUnI CLI

```A CLI tool to package the app module to a ready-for-publish zip file```

### Install the dependency

`yarn add --dev https://chw502@bitbucket.org/chw502/ituni-cli.git`

### Run
`ituni package -d <path/to/www/folder>`
